import pymysql
import os
import ijson

def getData():
    rds_host = os.environ['DB_HOST']
    user_name = os.environ['DB_USER_NAME']
    password= os.environ['DB_PASSWORD']
    db_name = os.environ['DB_NAME']
    conn = pymysql.connect(rds_host, user=user_name, passwd=password, db=db_name, connect_timeout=10)
    with conn.cursor() as cur:
        cur.execute("select * from Sample")
        return cur.fetchall()

def hello(event, context):
    print(getData())
    response = {
        "statusCode": 200,
        "body": "hello from python"
    }

    return response

