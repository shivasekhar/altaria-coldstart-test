import json
import sys

from os import listdir
from os.path import isfile, join

def get_data(filename):
    result = {}
    result["data"] = []

    with open(filename) as f:
        data = json.load(f)
        for doc in data:
            result['Id'] = doc['Id']
            segments = doc.get("Segments")
            if segments:
                for segment in segments:
                    document = segment.get('Document')
                    document = json.loads(document)
                    if document:
                        subsegments = document.get("subsegments")
                        if subsegments is None:
                            continue
                        for subsegment in subsegments:
                            if 'start_time' in subsegment.keys() and 'end_time' in subsegment.keys():
                                seg_data = {}
                                seg_data['time'] = subsegment['end_time'] - subsegment['start_time']
                                seg_data['event'] = subsegment['name']
                                seg_data['doc_time'] = document['end_time'] - document['start_time']
                                result["data"].append(seg_data)
    if len(result["data"]) != 0:
        return result

function_dir = sys.argv[1]

trace_path=join(function_dir, ".trace/")

trace_files = [join(trace_path, f) for f in listdir(trace_path) if isfile(join(trace_path, f))]

results = []

for trace_file in trace_files:
    result = get_data(trace_file)
    results.append(result)

with open('output-{}.json'.format(function_dir.strip("/")), 'w') as f:
    json.dump(results, f)
print(results)
