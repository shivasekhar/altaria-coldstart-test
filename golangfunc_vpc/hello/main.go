package main

import (
	"context"
	"fmt"
	"os"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"

	"database/sql"

	_ "github.com/go-sql-driver/mysql"
)

type Response events.APIGatewayProxyResponse

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}
func getData() {
	username := os.Getenv("DB_USER_NAME")
	password := os.Getenv("DB_PASSWORD")
	dbName := os.Getenv("DB_NAME")
	dbHost := os.Getenv("DB_HOST")
	db, err := sql.Open("mysql", username+":"+password+"@tcp("+dbHost+")/"+dbName)
	checkErr(err)
	res, err := db.Query("Select * from Sample")
	checkErr(err)
	fmt.Println(res)
}

// Handler is our lambda handler invoked by the `lambda.Start` function call
func Handler(ctx context.Context) (Response, error) {
	getData()
	resp := Response{
		StatusCode:      200,
		IsBase64Encoded: false,
		Body:            "hello from golang",
	}

	return resp, nil
}

func main() {
	lambda.Start(Handler)
}
