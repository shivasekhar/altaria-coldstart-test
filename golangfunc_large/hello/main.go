package main

import (
	"context"
	"fmt"
	"os"
"github.com/spf13/viper"
"github.com/spf13/cobra"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"

	"database/sql"

	log "github.com/Sirupsen/logrus"
	_ "github.com/go-sql-driver/mysql"
)

type Response events.APIGatewayProxyResponse

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

var rootCmd = &cobra.Command{
		Use:  "mycli-full",
			Long: `SDK Use Demo`,
		}

func getData() {
	viper.BindPFlag("er", rootCmd.PersistentFlags().Lookup("url"))
	os.Getenv("DB_USER_NAME")
	db, err := sql.Open("mysql", "altariasandbox:altariasandbox-pwd@tcp(altaria-sandbox-inst.ctlzupcrhjz4.us-east-1.rds.amazonaws.com:3306)/lambda_test")
	checkErr(err)
	res, err := db.Query("Select * from Sample")
	checkErr(err)
	fmt.Println(res)

}

// Handler is our lambda handler invoked by the `lambda.Start` function call
func Handler(ctx context.Context) (Response, error) {
	log.SetLevel(log.DebugLevel)
	getData()
	resp := Response{
		StatusCode:      200,
		IsBase64Encoded: false,
		Body:            "hello from golang",
	}

	return resp, nil
}

func main() {
	lambda.Start(Handler)
}
