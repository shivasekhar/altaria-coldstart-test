setopt shwordsplit

function_path=$1

cd $function_path
mkdir .trace
npm i

for i in {1..30}
do
	echo "Starting deployement $i"

	bash build.sh

	echo "fetching endpoint"

	data=$(sls info --app | awk '/endpoints:/{getline; print} /functions:/{getline; print}')

	endpoint=$(echo $data | cut -d' ' -f3)
	function_name=$(echo $data | cut -d' ' -f5)

	echo "Endpoint $endpoint"
	echo "Function $function_name"

	echo "Hitting endpoint"
	curl $endpoint 

	sleep 5

	echo ""
	EPOCH=$(date +%s)

	TRACE_IDS=$(aws xray get-trace-summaries --start-time $(($EPOCH-3600)) --end-time $(($EPOCH)) --query "TraceSummaries[?EntryPoint.Name=='$function_name'].Id" --output text)

	echo $TRACE_IDS | tr ' ' '\n' | while read line ; do echo "$line processing"; aws xray batch-get-traces --trace-ids $line --query 'Traces[*]' > ./.trace/$line ; done
	
	sls remove
done

cd -
