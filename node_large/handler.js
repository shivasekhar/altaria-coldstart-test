'use strict';

var mysql      = require('mysql');
const csv = require('fast-csv');

function getData() {
	return new Promise( function (resolve, reject) {
		var connection = mysql.createConnection({
			  host     : process.env.DB_HOST,
			  user     : process.env.DB_USER_NAME,
			  password : process.env.DB_PASSWORD,
			  database : process.env.DB_NAME
		});

		connection.connect();

		connection.query('select * from Sample', function (error, results, fields) {
			if (error) {
				reject(error);
			}
			connection.end();
			resolve(results)
		});
	});
}


module.exports.hello = async event => {
	var data = await getData();

  	return {
    	statusCode: 200,
    	body: JSON.stringify(
      	{
        	message: 'Hello from Node ' + data[0]["data"],
      	},
      	null,
      	2
    	),
  	};

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};
